// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
function product(url, title, name, cost, button) {
  return {
    url: url,
    title: title,
    name: name,
    cost: cost,
    button: button,
  };
}
const prd1 = product(
  "product.jpg",
  "Maks boks populyarniy",
  "info",
  35000,
  "vibrat"
);
const prd3 = product(
  "product.jpg",
  "Maks boks traditsiya",
  "info",
  29000,
  "vibrat"
);
const prd2 = product("product.jpg", "Maks boks retro", "info", 30000, "+ 2 -");
const prd4 = product("product.jpg", "Maks boks trend", "info", 3000, "+ 1 -");
console.log(prd1);
console.log(prd2);
console.log(prd3);
console.log(prd4);
// You code here ...

// Constructor Function
function Product(url, title, name, cost, button) {
  this.url = url;
  this.title = title;
  this.name = name;
  this.cost = cost;
  this.button = button;
}

const pr1 = new Product(
  "product.jpg",
  "Maks boks populyarniy",
  "info",
  35000,
  "vibrat"
);
const pr2 = new Product(
  "product.jpg",
  "Maks boks retro",
  "info",
  30000,
  "+ 2 -"
);
const pr3 = new Product(
  "product.jpg",
  "Maks boks traditsiya",
  "info",
  29000,
  "vibrat"
);
const pr4 = new Product(
  "product.jpg",
  "Maks boks trend",
  "info",
  3000,
  "+ 1 -"
);
console.log(typeof pr1);
console.log(typeof pr2);
console.log(typeof pr3);
console.log(typeof pr4);
// You code here ...
