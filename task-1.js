// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// ! You should not use case, switch, or if statements

// Setup
const lookUp = {
  // Only change code below this line
  alpha: "Adams",
  bravo: "Boston",
  charlie: "Chicago",
  delta: "Denver",
  echo: "Easy",
  foxtrot: "Frank",
};

// Only change code above this line

console.log(lookUp.charlie);
console.log(lookUp.alpha);
console.log(lookUp.bravo);
console.log('');
// phoneticLookup("charlie"); // Chicago
// phoneticLookup("alpha"); // Adams
// phoneticLookup("bravo"); // Boston
// phoneticLookup(""); // undefined
